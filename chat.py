from api_key import *
import os
import openai

openai.api_key = OPENAI_KEY
completion = openai.Completion()
chat_log = '''Human: Hello, who are you?
AI: I am doing great. How can I help you today?
'''

def ask(question, chat_log):
    response = completion.create(
        prompt=f'{chat_log}\nHuman: {question}\nAI:',
        engine='text-davinci-003',
        stop=['\nHuman'],
        temperature=0.8,
        top_p=0.2,
        frequency_penalty=0,
        presence_penalty=0.6,
        best_of=1,
        max_tokens=500
    )
    return response.choices[0].text.strip()

while True:
    question = input('Human:')
    chat_log = f'{chat_log}Human: {question}\n'
    answer = ask(f'Human:{question}', chat_log)
    print(answer)
    chat_log = f'{chat_log}AI: {answer}\n'