# chatbot

TP Python M1 ESGI Grenoble

## Installation

```bash
cd <ou tu veux>
git clone https://gitlab.com/arrobe/chatbot.git
cd chatbot
python -m venv chatbot
source chatbot/bin/activate
(chatbot) $ pip install openai
```

Il faut ensuite ouvrir un compte gratuit chez OpenAI
& récupérer une clé d'API

Il faut créer un fichier api_key.py à la racine du projet
et y ajouter la clé d'API sous la forme

```python
OPENAI_KEY = "sk-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
```

## Lancement
```bash
python chat.py
```